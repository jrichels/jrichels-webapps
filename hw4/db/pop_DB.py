# IMPROVEMENTS
# Better handling of empty text fields
# Content types other than 'ITEM'
# Find better way of handling 'u200b' zero-width space

import json
import logging
import mysql.connector

logging.basicConfig(filename = 'mysql.log', level = logging.DEBUG, format = '%(asctime)s %(message)s')

DAYS_DICT = {
	'Sunday' :    'U',
	'Monday' :    'M',
	'Tuesday' :   'T',
	'Wednesday' : 'W',
	'Thursday' :  'R',
	'Friday' :    'F',
	'Saturday' :  'S'
}

# database connection parameters
DB_USER = 'root'
DB_HOST = '127.0.0.1'
DB_NAME = 'feednd'

# connect to feednd database and create cursor USING utf8mb4
DB_cnx = mysql.connector.connect(user = DB_USER, host = DB_HOST, database = DB_NAME)
DB_curs = DB_cnx.cursor()
DB_curs.execute('SET NAMES `utf8mb4`')
DB_curs.execute('SET CHARACTER SET `utf8mb4`')
DB_curs.execute('SET character_set_connection = `utf8mb4`')

# load restaurant data JSON into dictionary
input_f = open('restaurant_data.json', 'r')
restaurants = json.load(input_f)
input_f.close()

# loop through restaurants, inserting into restaurants table
for restaurant_ID, restaurant_dict in restaurants.iteritems():
	restaurant_inp_dict = {
		'restaurant_PK' : restaurant_ID,
		'name' :  restaurant_dict['name'],
		'street_address' : restaurant_dict['street_address'],
		'city' :  restaurant_dict['locality'],
		'state' : restaurant_dict['region'],
		'zip' :   restaurant_dict['postal_code'],
		'url' :   restaurant_dict['website_url'],
		'phone' : restaurant_dict['phone'],
		'facebook_url' : restaurant_dict.get('facebook_url'),
		'twitter_id' :   restaurant_dict.get('twitter_id'),
		'latitude' :     restaurant_dict['lat'],
		'longitude' :    restaurant_dict['long']
	}
	add_restaurant_qry = 'INSERT INTO `restaurants_t` (`restaurant_PK`, `name`, `street_address`, `city`, `state`, `zip`, `url`, `phone`, `facebook_url`, `twitter_id`, `latitude`, `longitude`) VALUES (%(restaurant_PK)s, %(name)s, %(street_address)s, %(city)s, %(state)s, %(zip)s, %(url)s, %(phone)s, %(facebook_url)s, %(twitter_id)s, %(latitude)s, %(longitude)s)'
	DB_curs.execute(add_restaurant_qry, restaurant_inp_dict)

	# loop through menus, inserting into menus table
	for menu_dict in restaurant_dict['menus']:
		menu_inp_dict = {
			'restaurant_FK' : restaurant_ID,
			'name' : menu_dict['menu_name']
		}
		add_menu_qry = 'INSERT INTO `menus_t` (`restaurant_FK`, `name`) VALUES (%(restaurant_FK)s, %(name)s)'
		DB_curs.execute(add_menu_qry, menu_inp_dict)
		menu_ID = DB_curs.lastrowid

		# loop through sections, inserting into sections table
		for section_dict in menu_dict['sections']:
			section_inp_dict = {
				'menu_FK' : menu_ID,
				'name' : 'Section'
#				'name' : section_dict['section_name'].encode('utf-8', 'ignore')
			}
			add_section_qry = 'INSERT INTO `sections_t` (`menu_FK`, `name`) VALUES (%(menu_FK)s, %(name)s)'
			DB_curs.execute(add_section_qry, section_inp_dict)
			section_ID = DB_curs.lastrowid

			# loop through subsections, inserting into subsections table
			for subsection_dict in section_dict['subsections']:
				subsection_inp_dict = {
					'section_FK' : section_ID,
					'name' : subsection_dict['subsection_name']
				}
				add_subsection_qry = 'INSERT INTO `subsections_t` (`section_FK`, `name`) VALUES (%(section_FK)s, %(name)s)'
				DB_curs.execute(add_subsection_qry, subsection_inp_dict)
				subsection_ID = DB_curs.lastrowid

				# loop through contents, inserting into items table if content is item
				for content_dict in subsection_dict['contents']:
					# check that content is item
					if content_dict['type'] == 'ITEM':
						# add dummy price of $2 if item missing price
						if not content_dict.get('price'):
							content_dict['price'] = 2
						item_inp_dict = {
							'subsection_FK' : subsection_ID,
							'name' : content_dict['name'],
							'price' : content_dict['price'],
							'description' : content_dict.get('description', '')
						}
						add_item_qry = "INSERT INTO `items_t` (`subsection_FK`, `name`, `description`, `price`) VALUES (%(subsection_FK)s, %(name)s, %(description)s, %(price)s)"
						# allow for possibility of clash (happens often with Chinese food)
						try:
							DB_curs.execute(add_item_qry, item_inp_dict)
						except:
							pass

	# loop through hours, inserting into hours table
	for day, hours_list in restaurant_dict['open_hours'].iteritems():
		for hour in hours_list:
			start_time = hour[:8]
			end_time = hour[-8:]
			inp_dict = {
				'restaurant_FK' : restaurant_ID,
				'day' : DAYS_DICT[day],
				'open' : start_time,
				'close' : end_time
			}
			add_hour = "INSERT INTO hours_t (restaurant_FK, day, open, close) VALUES (%(restaurant_FK)s, %(day)s, %(open)s, %(close)s)"
			DB_curs.execute(add_hour, inp_dict)

# add test user
DB_curs.execute("INSERT INTO `users_t` (`name`, `email`) VALUES ('Jon', 'jon@jon.com')")

# add test order
DB_curs.execute('INSERT INTO `orders_t` (`user_FK`, `placed`) VALUES (1, 0)')

# commit and close database connection
DB_cnx.commit()
DB_cnx.close()
