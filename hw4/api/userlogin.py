import cherrypy
import apiutil
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
from apiutil import errorJSON
from passlib.apps import custom_app_context as pwd_context
from config import conf

SESSION_KEY = '_cp_username'

class UserLogin(object):
    exposed = True

    @cherrypy.tools.json_in(force=False)
    def POST(self):
        ''' login existing user
        should only be used through SSL connection
        expects JSON:
        { 'username' : username,
          'password' : password
        }
        return JSON:
        { 'errors' : [] }
        error code / message:
        5000 : Expected username and password
        5001 : Incorrect username or password
        '''
        try:
            username = cherrypy.request.json["username"]
            password = cherrypy.request.json["password"]
        except:
            return errorJSON(code=5000, message="Expected username and password")
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='FeedND',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="select password from users where email=%s";
        cursor.execute(q,(username,))
        r=cursor.fetchall()
        if len(r) == 0:
            return errorJSON(code=5001, message="Incorrect username or password")
        hash=r[0][0]
        match=pwd_context.verify(password,hash)
        print "Password %s \nHash: %s \nmatch %s" % (password,hash,match)
        password=""
        if not match:
            # username / hash do not exist in database
            return errorJSON(code=5001, message="Incorrect username or password")
        else:
            # username / password correct
            cherrypy.session.regenerate()
            print "Login SESSION KEY % s" % SESSION_KEY
            cherrypy.session[SESSION_KEY] = cherrypy.request.login = username
            result={'errors':[]}
            return json.dumps(result)

application = cherrypy.Application(UserLogin(), None, conf)
