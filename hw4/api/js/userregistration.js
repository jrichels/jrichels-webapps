$(function() {
  function isValidEmail(email) {
    var re = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    return re.test(email);
  }

  function isValidPassword(pwd) {
    // at least one number, one lowercase, one uppercase letter, one special symbol
    // at least nine characters
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@-]).{9,}/;
    return re.test(pwd);
  }

  function isValidPasswordConfirm(pwd, pwd_confirm) {
    // password and password confirmation must match
    return (pwd === pwd_confirm);
  }

  function isValidPhone(phone) {
    // phone must be all digits
    var re = /^\d*$/;
    return re.test(phone);
  }

  $('#user-add-form').on('submit', function(event) { // form id

    var email = $('#user-email-input').val(); // input id
    var pwd = $('#user-password-input').val(); // input id
    var pwd_confirm = $('#user-password-confirm-input').val() // input id
    var phone = $('#user-phone-input').val(); // input id
    $('#user-password-confirm-input').remove();

    if(isValidEmail(email)) {
      $('#email-error').hide(); // div id
    } else {
      $('#email-error').text('Email must be in the correct format.').show();
      event.preventDefault();
    }

    if(isValidPassword(pwd)) {
      $('#password-error').hide(); // div id
    } else {
      $('#password-error').text('Password has to be 9 or more characters, and contain at least 1 upper case, 1 lower case, 1 number, and 1 symbol.').show();
      event.preventDefault();
    }

    if(isValidPasswordConfirm(pwd, pwd_confirm)) {
      $('#password-confirm-error').hide(); // div id
    } else {
      $('#password-confirm-error').text('Passwords do not match.').show();
      event.preventDefault();
    }

    if(isValidPhone(phone)) {
      $('#phone-error').hide(); // div id
    } else {
      $('#phone-error').text('Phone must be all digits.').show();
      event.preventDefault();
    }
  });
});

