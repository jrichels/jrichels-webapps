import json
import requests

s = requests.Session()
host = 'http://ec2-52-0-248-152.compute-1.amazonaws.com'
s.headers.update({'Accept': 'application/json'})

print 'Testing PUT /orders/1/item/255'
r = s.put(host + '/orders/1/item/255', data = json.dumps({'quantity' : 4}))
print r.status_code
if r.status_code == requests.codes.ok:
	print r.json()

print 'Testing GET /orders/1/items/255'
r = s.get(host + '/orders/1/items/255')
print r.status_code
if r.status_code == requests.codes.ok:
	print r.json()
