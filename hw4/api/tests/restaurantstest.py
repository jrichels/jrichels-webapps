import requests
s = requests.Session()
host = 'http://ec2-52-0-248-152.compute-1.amazonaws.com'
s.headers.update({'Accept': 'application/json'})

print 'Testing GET /restaurants'
r = s.get(host + '/restaurants')
print r.status_code
if r.status_code == requests.codes.ok:
	print r.json()

print 'Testing GET /restaurants/00dae7092df41d0f8df2/categories'
r = s.get(host + '/restaurants/00dae7092df41d0f8df2/categories')
print r.status_code
if r.status_code == requests.codes.ok:
	print r.json()
