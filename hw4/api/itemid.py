''' Controller for /restaurants{restID}/categories/{catID}/items/{itemID}'''
import cherrypy
class ItemID(object):
    ''' Handles resources /items/{restID}/{id}
        Allowed methods: GET, PUT, DELETE, OPTIONS '''
    exposed = True

    def GET(self, restID, catID, itemID):
        return "GET /restaurants{restID=%s}/categories/{catID=%s}/items/{itemID=%s}   ...   ItemID.GET" % (restID,catID,itemID)

    def PUT(self, restID, catID, itemID, **kwargs):
        result = "PUT /restaurants{restID=%s}/categories/{catID=%s}/items/{itemID=%s}   ...     ItemID.PUT\n" % (restID,catID,itemID)
        result += "PUT /restaurants{restID=%s}/categories/{catID=%s}/items/{itemID=%s} body:\n" % (restID,catID,itemID)
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update restaurant
        # Prepare response
        return result

    def DELETE(self, restID, catID, itemID):
        #Validate id
        #Delete restaurant
        #Prepare response
        return "DELETE /restaurants{restID=%s}/categories/{catID=%s}/items/{itemID=%s}   ...   ItemID.DELETE" % (restID,catID,itemID)

    def OPTIONS(self, restID, catID, itemID):
        #Prepare response
        return "<p>/restaurants{restID}/categories/{catID}/items/{itemID} allows GET, PUT, DELETE, and OPTIONS</p>"

