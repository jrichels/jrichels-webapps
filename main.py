import cherrypy
import collections

class ExampleApp(object):
   @cherrypy.expose
   def index(self):
      page = """
      <!DOCTYPE html>
      <html>

      <head>
      <title>ND Gastronomy</title>
      <style>
      ul {
         list-style-type: none;
         margin: 0;
         padding: 0;
      }
      li {
      display: inline;
      }
      </style>
      <head>

      <body>
      <h2>ND Gastronomy</h2>

      <ul>
      <li><a href="orders">Orders</a></li>
      <li><a href="restaurants">Restaurants</a></li>
      <li><a href="account">Account</a></li>
      </ul>

      <table>
         <tr>
            <td>Restaurant</td>
            <td>Distance</td>
         </tr>
      """

      rests = collections.OrderedDict()
      rests["Subway"] = 427
      rests["O'Rourke's Public House"] = 632
      rests["The Mark Dine & Tap"] = 730

      for k, v in rests.items():
         page += "         <tr>"
         page += "            <td>" + k + "</td>"
         page += "            <td>" + str(v) + "</td>"
         page += "         </tr>"

      page += """
      </table>

      </body>
      </html>
      """

      return page

application = cherrypy.Application(ExampleApp(), None)
