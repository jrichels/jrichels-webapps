from decimal import *
import json
import mysql.connector
import random

days_dict = {
	'Sunday' :    'U',
	'Monday' :    'M',
	'Tuesday' :   'T',
	'Wednesday' : 'W',
	'Thursday' :  'R',
	'Friday' :    'F',
	'Saturday' :  'S'
}

# database connection parameters
DB_USER = 'root'
DB_HOST = '127.0.0.1'
DB_NAME = 'feednd'

# connect to feednd database and create cursor
DB_cnx = mysql.connector.connect( user = DB_USER, host = DB_HOST, database = DB_NAME )
DB_curs = DB_cnx.cursor()

# load restaurant data JSON into dictionary
input_f = open( 'restaurantData.json', 'r' )
restaurantDict = json.load( input_f )
input_f.close()

# loop through restaurants, inserting into restaurants table
for restaurant_ID, restaurant in restaurantDict.iteritems():
	inp_dict = {
		'restaurant_PKID' : restaurant_ID,
		'name' :  restaurant.get('name'),
		'street_address' : restaurant.get('street_address'),
		'city' :  restaurant.get('locality'),
		'state' : restaurant.get('region'),
		'zip' :   restaurant.get('postal_code'),
		'url' :   restaurant.get('website_url'),
		'phone' : restaurant.get('phone'),
		'facebook_url' : restaurant.get('facebook_url'),
		'twitter_id' :   restaurant.get('twitter_id'),
		'latitude' :     restaurant.get('lat'),
		'longitude' :    restaurant.get('long')
	}

	add_restaurant =  "INSERT INTO restaurants_t (restaurant_PKID, name, street_address, city, state, zip, url, phone, facebook_url, twitter_id, latitude, longitude) VALUES (%(restaurant_PKID)s, %(name)s, %(street_address)s, %(city)s, %(state)s, %(zip)s, %(url)s, %(phone)s, %(facebook_url)s, %(twitter_id)s, %(latitude)s, %(longitude)s)"
	DB_curs.execute( add_restaurant, inp_dict )
	
	# loop through hours, inserting into hours table
	for day, hours_list in restaurant['open_hours'].iteritems():
		for hour in hours_list:
			start_time = hour[:8]
			end_time = hour[-8:]
			
			inp_dict = {
				'restaurant_FK' : restaurant_ID,
				'day' : days_dict[day],
				'open' : start_time,
				'close' : end_time
			}

			add_hour = "INSERT INTO hours_t (restaurant_FK, day, open, close) VALUES (%(restaurant_FK)s, %(day)s, %(open)s, %(close)s)"
			DB_curs.execute( add_hour, inp_dict )

	# loop through menus
	for menu in restaurant['menus']:
		for section in menu['sections']:
			for subsection in section['subsections']:
				for content in subsection['contents']:
					if content['type'] == 'ITEM':
						if not content.get('price'):
							content['price'] = 2
						
						inp_dict = {
							'restaurant_FK' : restaurant_ID,
							'name' : content['name'],
							'price' : content['price']
						}

						add_item = "INSERT INTO items_t (restaurant_FK, name, price) VALUES (%(restaurant_FK)s, %(name)s, %(price)s)"
						try:
							DB_curs.execute( add_item, inp_dict )
						except Exception:	
							pass
# commit and close database connection
DB_cnx.commit()
DB_cnx.close()
