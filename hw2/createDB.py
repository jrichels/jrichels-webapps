import mysql.connector

# database connection parameters
DB_USER = 'root'
DB_HOST = '127.0.0.1'
DB_NAME = 'feednd'

# connect to feednd database and create cursor
DB_cnx = mysql.connector.connect( user = DB_USER, host = DB_HOST )
DB_curs = DB_cnx.cursor()

# create DB if does not exist
create_DB = "CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1" % DB_NAME
DB_curs.execute( create_DB )

# switch to feednd DB
use_DB =  "USE `%s`" % DB_NAME
DB_curs.execute( use_DB )

# drop all tables
DB_curs.execute( "DROP TABLE IF EXISTS hours_t" )
DB_curs.execute( "DROP TABLE IF EXISTS items_t" )
DB_curs.execute( "DROP TABLE IF EXISTS restaurants_t" )

# create tables
DB_curs.execute( '''CREATE TABLE restaurants_t (
	restaurant_PKID VARCHAR(20) NOT NULL,
	name VARCHAR(45) NOT NULL,
	street_address VARCHAR(100) NOT NULL,
	city VARCHAR(45) NOT NULL,
	state VARCHAR(2) NOT NULL,
	zip VARCHAR(5) NOT NULL,
	url VARCHAR(100) NOT NULL,
	phone VARCHAR(14) NOT NULL,
	facebook_url VARCHAR(100) NULL,
	twitter_id VARCHAR(45) NULL,
	latitude DECIMAL(8,6) NOT NULL,
	longitude DECIMAL(8,6) NOT NULL,
	PRIMARY KEY(restaurant_PKID));''' )

DB_curs.execute( '''CREATE TABLE hours_t (
	restaurant_FK  VARCHAR(20) NOT NULL,
	day enum ('U','M','T','W','R','F','S') NOT NULL,
	open TIME NOT NULL,
	close TIME NOT NULL,
	PRIMARY KEY(restaurant_FK,day,open),
	FOREIGN KEY(restaurant_FK) REFERENCES restaurants_t(restaurant_PKID)
	ON DELETE CASCADE);''' )

DB_curs.execute( '''CREATE TABLE items_t (
	restaurant_FK VARCHAR(20) NOT NULL,
	name VARCHAR(45) NOT NULL,
	price DECIMAL(4,2) NOT NULL,
	PRIMARY KEY(restaurant_FK,name),
	FOREIGN KEY(restaurant_FK) REFERENCES restaurants_t(restaurant_PKID)
	ON DELETE CASCADE);''' )

# commit and close database connection
DB_cnx.commit()
DB_cnx.close()
