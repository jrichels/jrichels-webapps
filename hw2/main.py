import cherrypy
import mysql.connector

class FeedND(object):
   @cherrypy.expose
   def index(self):
      # database connection parameters
      DB_USER = 'root'
      DB_HOST = '127.0.0.1'
      DB_NAME = 'feednd'

      # connect to feednd database and create cursor
      DB_cnx = mysql.connector.connect( user = DB_USER, host = DB_HOST, database = DB_NAME )
      DB_curs = DB_cnx.cursor()

      page = """
      <!DOCTYPE html>
      <html>

      <head>
      <title>ND Gastronomy</title>
      <style>
      ul {
         list-style-type: none;
         margin: 0;
         padding: 0;
      }
      li {
      display: inline;
      }
      </style>
      <head>

      <body>
      <h2>ND Gastronomy</h2>

      <ul>
      <li><a href="orders">Orders</a></li>
      <li><a href="restaurants">Restaurants</a></li>
      <li><a href="account">Account</a></li>
      </ul>

      <table>
         <tr>
            <td>Restaurant</td>
            <td>Address</td>
            <td>Website</td>
            <td>Phone</td>
         </tr>
      """

      DB_curs.execute("SELECT name,street_address,city,url,phone FROM restaurants_t")
      rows = DB_curs.fetchall()

      for row in rows:
         page += '         <tr>'
         page += '            <td>' + row[0] + '</td>'
         page += '            <td>' + row[1] + ', ' + row[2] + '</td>'
         page += '            <td>' + row[3] + '</td>'
         page += '            <td>' + row[4] + '</td>'
         page += '         </tr>'

      page += """
      </table>

      </body>
      </html>
      """

      return page

application = cherrypy.Application(FeedND(), None)
