This directory contains the implementation of the feednd.com api

This version demonstrates the use of a build tool called Gulp. You can think
of Gulp as a kind of Makefile for front-end web development. Very similar to
Makefile, you define a set of commands in a file named `gulpfile.js`, which can
be run from the command line using

    % gulp <name-of-command>

You can see that this gulpfile is quite short. It only defines three commands:
`less`, `watch`, and `default` (which runs when no command name is given).
Unlike Makefile, Gulp does not define commands in terms of targets,
dependencies, and shell commands. Instead, it simply associates the name of a
command with a function to be run. You don't really need to understand what's
going on inside those functions for now. Just know that when you run
`gulp less`, it generates a file named `main.css` and places it in the `css/`
directory.

Why use Gulp? So we can use Less. Less is an extension to the CSS language
which offers some improvements to the standard syntax like variables, nested
rules, and mixins. Obviously, browsers do not accept Less code as-is. So how
does it work? Less is implemented as a compiler (written in JavaScript) which
converts Less code to good old CSS. You can either include the less.js compiler
on your web page along with your .less stylesheets, or (the better way) you can
pre-compile your Less source code to CSS and include the result in your page
instead. We follow the latter option.

Why use Less? Besides the fact that it's a nice alternative to writing plain
CSS, it's also the language that Bootstrap's stylesheets are written in. The
fact that Less has features like variables and mixins allows Bootstrap to be
themeable -- by overriding a few variables, you can change the color of, say,
the header bar of your site. This is a much more maintainable approach to
writing large, complex stylesheets. You can find a reference of overridable
Bootstrap variables at <http://getbootstrap.com/customize/#less-variables>.

You'll need to install these build tools and plugins separately, as they are
not included in the repository. That's where the file `package.json` comes in
(another configuration file! hooray!). This file serves the same purpose as
`Gemfile` from the Ruby on Rails world. It declares the names and version
numbers of all third-party packages which our project requires. Where
`gulpfile.js` is used by the `gulp` command, `package.json` is used by the
`npm` command. NPM is the package manager for Node.js. We use NPM to install
the Gulp, Less, and Boostrap packages we need. This practice of declaring
software dependencies rather than including third-party code directly in our
code-base is useful when the size of those libraries is large (and they can be
quite massive). There are other benefits to using npm and the Node.js package
system as well, but that's its own separate topic.

You will need to install both Node.js and `npm` on your system in order to run
`gulp`. Once `npm` is installed, run `npm install` inside the project
directory. This will download the project's required packages to a directory
named `node_modules`. Note that because everything is downloaded to a local
directory, you don't need to (and should not) run the command with any special
permissions (no `sudo`). This takes care of everything except for the `gulp`
command itself, which can be installed by running `npm install --global gulp`
(and for this one you will probably need `sudo`).

It took a bit of setup work, but at this point you should be able to compile
the project's `main.css` file from the Less source code under the `less`
directory by running `gulp less`.

Something interesting happens when you run `gulp watch`. This tells Gulp to
monitor the file system for changes to the .less files under `less/` and re-run
`gulp less` whenever any of those files changes. This is a convenient way to
keep your compiled CSS automatically up-to-date while working. You can get even
more advanced than this -- there are plugins for live-reload, which can refresh
a web page in the browser every time its source code changes.

If you would like to explore any of these tools further, you can visit their
respective websites. They all have very good documentation and setup tutorials.
They're only a search away, but here are some links for the sake of
completeness:

* Gulp: <http://gulpjs.com/>
* Grunt (an alternative to Gulp): <http://gruntjs.com/>
* Bootstrap: <http://getbootstrap.com/>
* Less: <http://lesscss.org/>
* Sass (an alternative to Less): <http://sass-lang.com/>
* NPM: <https://www.npmjs.com/>
* Node.js: <http://nodejs.org/>
* Explanation of Node.js modules: <http://nodejs.org/api/modules.html>

Commands for installing and compiling in AWS ec2
------------------------------------------------

To install Node.js in your AWS ec2 instance, use this command:
sudo yum install nodejs npm --enablerepo=epel

After success, you should be able to look for npm:
which npm

Obtain dependencies:
npm install

This will create a node_modules directory.
Now, install gulp:

sudo npm install --global gulp

Compile less:
gulp less

Directory Contents
------------------

-- restaurants1.py : example /restaurants handler using method dispatcher

-- restaurants2.py : /restaurants handler using method dispatcher
                   : also uses Jinja2 templates in templates directory
                   : uses css/themes.css for selecting items
                   : uses other css for bootstrap themes
                   : uses js for jquery and bootstrap
-- main.py         : example app to show mounting multiple apps / handlers
-- config/httpd.conf  :  Apache configuration to run as WSGI daemon app restaurants2.py and main.py
-- config/mysql.conf  : Configuration for MySQL when not running through WSGI
                        (under [mysql], define user, password, name, and host)
-- templates/restaurants-tmpl.html 
-- templates/categories-tmpl.html 
-- templates/items-tmpl.html 
-- gulpfile.js    : Configuration file for Gulp, which for now compiles .less
                    files into .css
-- package.json   : Configuration file for npm, which fetches third-party code
                    needed for Gulp and Less
-- less/          : Directory containing Less code
-- less/main.less : Main Less file which includes all the others

