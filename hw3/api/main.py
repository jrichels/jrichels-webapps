import cherrypy
import sys
import mysql.connector

class ExampleApp(object):
    @cherrypy.expose
    def index(self,name=None):
        if name:
            return "<h1>hello %s " % (name) + sys.version + "</h1>"
        else:
            return "<h1>hello world "  + sys.version + "</h1>"
    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='test', password='mypass',
                              host='127.0.0.1',
                              database='testdb')
        cursor = cnx.cursor()
        query = ("SELECT firstname,lastname,email FROM Invitations")
        cursor.execute(query)
        info = str()
        print cursor
        for (firstname, lastname, email) in cursor:
           info = info + "Full Name:" + lastname + firstname + "Email: "+email
        return info
application = cherrypy.Application(ExampleApp(), None)

if __name__ == '__main__':
    import os.path
    tutconf = os.path.join(os.path.dirname(__file__), 'tutorial.conf')
    application = cherrypy.quickstart(ExampleApp(), config=tutconf)
