import mysql.connector

# database connection parameters
DB_USER = 'root'
DB_HOST = '127.0.0.1'
DB_NAME = 'feednd'

# connect to feednd database and create cursor USING utfmb4
DB_cnx = mysql.connector.connect(user = DB_USER, host = DB_HOST)
DB_curs = DB_cnx.cursor()
DB_curs.execute('SET NAMES `utf8mb4`')
DB_curs.execute('SET CHARACTER SET `utf8mb4`')
DB_curs.execute('SET character_set_connection = `utf8mb4`')

# create DB if does not exist
create_DB = 'CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET `utf8mb4`'
DB_curs.execute(create_DB % DB_NAME)

# switch to feednd DB
use_DB = 'USE %s'
DB_curs.execute(use_DB % DB_NAME)

# drop all tables
DB_curs.execute('DROP TABLE IF EXISTS `lineitems_t`')
DB_curs.execute('DROP TABLE IF EXISTS `orders_t`')
DB_curs.execute('DROP TABLE IF EXISTS `users_t`')
DB_curs.execute('DROP TABLE IF EXISTS `items_t`')
DB_curs.execute('DROP TABLE IF EXISTS `subsections_t`')
DB_curs.execute('DROP TABLE IF EXISTS `sections_t`')
DB_curs.execute('DROP TABLE IF EXISTS `menus_t`')
DB_curs.execute('DROP TABLE IF EXISTS `hours_t`')
DB_curs.execute('DROP TABLE IF EXISTS `restaurants_t`')

# create tables
DB_curs.execute('''CREATE TABLE `restaurants_t` (
	`restaurant_PK`  VARCHAR(20)  CHARACTER SET `utf8mb4` NOT NULL,
	`name`           VARCHAR(100) CHARACTER SET `utf8mb4` NOT NULL,
	`street_address` VARCHAR(100) CHARACTER SET `utf8mb4` NOT NULL,
	`city`  VARCHAR(50)  CHARACTER SET `utf8mb4` NOT NULL,
	`state` VARCHAR(2)   CHARACTER SET `utf8mb4` NOT NULL,
	`zip`   VARCHAR(5)   CHARACTER SET `utf8mb4` NOT NULL,
	`url`   VARCHAR(100) CHARACTER SET `utf8mb4` NOT NULL,
	`phone` VARCHAR(14)  CHARACTER SET `utf8mb4` NOT NULL,
	`facebook_url` VARCHAR(100) CHARACTER SET `utf8mb4` NULL,
	`twitter_id`   VARCHAR(50)  CHARACTER SET `utf8mb4` NULL,
	`latitude`  DECIMAL(8,6) NOT NULL,
	`longitude` DECIMAL(8,6) NOT NULL,
	PRIMARY KEY(`restaurant_PK`))''')

DB_curs.execute('''CREATE TABLE `menus_t` (
	`menu_PK`       INT NOT NULL AUTO_INCREMENT,
	`restaurant_FK` VARCHAR(20) CHARACTER SET `utf8mb4` NOT NULL,
	`name`          VARCHAR(50) CHARACTER SET `utf8mb4` NOT NULL,
	PRIMARY KEY(`menu_PK`),
	FOREIGN KEY(`restaurant_FK`) REFERENCES `restaurants_t`(`restaurant_PK`)
	ON DELETE CASCADE)''')
DB_curs.execute('ALTER TABLE `menus_t` ADD UNIQUE `UNIQ_name_restaurant_FK`(`name`, `restaurant_FK`)')

DB_curs.execute('''CREATE TABLE `sections_t` (
	`section_PK` INT NOT NULL AUTO_INCREMENT,
	`menu_FK`    INT NOT NULL,
	`name`       VARCHAR(50) CHARACTER SET `utf8mb4` NOT NULL,
	PRIMARY KEY(`section_PK`),
	FOREIGN KEY(`menu_FK`) REFERENCES `menus_t`(`menu_PK`)
	ON DELETE CASCADE)''')
#DB_curs.execute('ALTER TABLE `sections_t` ADD UNIQUE `UNIQ_name_menu_FK`(`name`, `menu_FK`)')

DB_curs.execute('''CREATE TABLE `subsections_t` (
	`subsection_PK` INT NOT NULL AUTO_INCREMENT,
	`section_FK`    INT NOT NULL,
	`name`          VARCHAR(50) CHARACTER SET `utf8mb4` NOT NULL,
	PRIMARY KEY(`subsection_PK`),
	FOREIGN KEY(`section_FK`) REFERENCES `sections_t`(`section_PK`)
	ON DELETE CASCADE)''')
DB_curs.execute('ALTER TABLE `subsections_t` ADD UNIQUE `UNIQ_name_section_FK`(`name`, `section_FK`)')

DB_curs.execute('''CREATE TABLE `items_t` (
	`item_PK`       INT NOT NULL AUTO_INCREMENT,
	`subsection_FK` INT NOT NULL,
	`name`          VARCHAR(50)  CHARACTER SET `utf8mb4` NOT NULL,
	`description`   VARCHAR(200) CHARACTER SET `utf8mb4` NULL,
	`price`         DECIMAL(4,2) NOT NULL,
	PRIMARY KEY(`item_PK`),
	FOREIGN KEY(`subsection_FK`) REFERENCES `subsections_t`(`subsection_PK`)
	ON DELETE CASCADE)''')

DB_curs.execute('''CREATE TABLE `hours_t` (
	`restaurant_FK`  VARCHAR(20) CHARACTER SET `utf8mb4` NOT NULL,
	`day`   ENUM('U','M','T','W','R','F','S') NOT NULL,
	`open`  TIME NOT NULL,
	`close` TIME NOT NULL,
	PRIMARY KEY(`restaurant_FK`, `day`, `open`),
	FOREIGN KEY(`restaurant_FK`) REFERENCES `restaurants_t`(`restaurant_PK`)
	ON DELETE CASCADE)''')

DB_curs.execute('''CREATE TABLE `users_t` (
	`user_PK` INT NOT NULL AUTO_INCREMENT,
	`name`    VARCHAR(50) CHARACTER SET `utf8mb4` NOT NULL,
	`email`   VARCHAR(50) CHARACTER SET `utf8mb4` NOT NULL,
	`password_hash` VARCHAR(120) CHARACTER SET `utf8mb4` NULL,
	`phone`         VARCHAR(14)  CHARACTER SET `utf8mb4` NULL,
	PRIMARY KEY(`user_PK`))''')
DB_curs.execute('ALTER TABLE `users_t` ADD UNIQUE `UNIQ_email`(`email`)')

DB_curs.execute('''CREATE TABLE `orders_t` (
	`order_PK`    INT NOT NULL AUTO_INCREMENT,
	`user_FK`     INT NOT NULL,
	`lastupdated` DATE NULL,
	`placed`      INT NOT NULL,
	PRIMARY KEY(`order_PK`),
	FOREIGN KEY(`user_FK`) references `users_t`(`user_PK`)
	ON DELETE CASCADE)''')

DB_curs.execute('''CREATE TABLE `lineitems_t` (
	`lineitem_PK` INT NOT NULL AUTO_INCREMENT,
	`order_FK`    INT NOT NULL,
	`item_FK`     INT NOT NULL,
	`quantity`    INT NOT NULL,
	PRIMARY KEY(`lineitem_PK`),
	FOREIGN KEY(`order_FK`) REFERENCES `orders_t`(`order_PK`)
	ON DELETE CASCADE,
	FOREIGN KEY(`item_FK`) REFERENCES `items_t`(`item_PK`)
	ON DELETE CASCADE)''')
DB_curs.execute('ALTER TABLE `lineitems_t` ADD UNIQUE `UNIQ_order_FK_item_FK`(`order_FK`, `item_FK`)')	

# commit and close database connection
DB_cnx.commit()
DB_cnx.close()
