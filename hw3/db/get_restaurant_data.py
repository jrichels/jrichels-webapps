import json
import sys
import urllib2

restaurantIDs = set(['00dae7092df41d0f8df2', '0559f40309a19b3949d5', '24419e195104ad57ba86', '4218edc2d112582bde38', '73a2615c8f11cf22ac84', '85cf71a488ac45e0012b', 'a2c367a73b6181fec216', 'ce682628b99cd9422e50', 'e3f1400c66b37c410723', 'f98dc7678100784d08c2', '0196cde7f90c65f47ab2', '4a1ecd9a41ed6402b8ba', '55ea8e237866103a26ef', '7009c6cc1083e4c6b3dd', 'f86eefdd5c83f1d31e5e', 'fec9589dfff42b2e6d10'])
out_dict = {}

for restaurantID in restaurantIDs:
	url = 'https://api.locu.com/v1_0/venue/%s/?api_key=a4bc7518cbd36aa547ac1bdbf3c3393696074aca' % restaurantID
	resp_dict = json.loads(urllib2.urlopen(url).read())
	out_dict[restaurantID] = resp_dict['objects'][0]

out_f = open('restaurant_data.json', 'w')
json.dump(out_dict, out_f)
out_f.close()
